---
title: "About Us"
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# about image
#image: "images/about/about-page.jpg"
# meta description
description : "Hypatia Helps is a transled non-profit aiming reducing unemployment and homelessneess for the trans community."
---

## ABOUT HYPATIA HELPS

Hypatia Helps is a nurturing, psychologically safe mentorship community, with an amazing code of conduct, that creates career opportunities for homeless and impoverished members of the transgender expereince. We provide mentorships, internships, and other programs.

### Values

In order to fulfill our mission we are guided by the following values, which bind our community governance process:

* **Equal Access** for all members of the trans community: We pledge to serve those who experience transmisogyny in all forms.
* **Honesty** in all forms of communication: We pledge to be both true and timely with all of our statements both publicly and internally to bring about effective change
* We will do our best to maintain **self sustainability** by avoiding unsustainable agreements or plans.
  * We are **responsible** for our own future and will be as self-sufficient as possible.
* We must embody **empathy**, we are an organization by and for trans people, and we do not intentionally bring about harm to the lives we affect. 
  * We will lend an ear to both similar and dissimilar **experiences** and consider them carefully.
* We strive to keep our processes and finances as public as possible by practice **Transparency**.
